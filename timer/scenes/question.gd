extends Label

var value1
var value2

var rng = RandomNumberGenerator.new()

var result
var math_operator

var random_button_value1
var rando_button_value2

var lucky_button

func questionair():
	rng.randomize()
	value1 = rng.randf_range(1,9)
	value2 = rng.randf_range(1,9)
	
	lucky_button = rng.randf_range(0,2)
	math_operator = rng.randf_range(0,2)
	
	if(int(math_operator) == 0):
		result = int(value1) + int(value2)
		set_text(str(int(value1))+"+"+ str(int(value2))) 
	if(int(math_operator) == 1):
		result = int(value1) - int(value2)
		set_text(str(int(value1))+"-"+ str(int(value2)))
	if(int(math_operator) == 2):
		result = int(value1) * int(value2)
		set_text(str(int(value1))+"*"+ str(int(value2)))
	
	if(int(lucky_button) == 0):
		get_node("../a1").is_lucky_button = true
		get_node("../a1").set_text(str(result))
		get_node("../a2").set_text(str(int(rand_range(1,9))))
		get_node("../a3").set_text(str(int(rand_range(1,9))))
	if(int(lucky_button) == 1):
		get_node("../a2").is_lucky_button = true
		get_node("../a2").set_text(str(result))
		get_node("../a1").set_text(str(int(rand_range(1,9))))
		get_node("../a2").set_text(str(int(rand_range(1,9))))
	if(int(lucky_button) == 2):
		get_node("../a3").is_lucky_button = true
		get_node("../a3").set_text(str(result))
		get_node("../a2").set_text(str(int(rand_range(1,9))))
		get_node("../a1").set_text(str(int(rand_range(1,9))))
	
	
