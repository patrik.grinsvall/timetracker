extends HTTPRequest


onready var cus_dropdown = get_node("../TextureRect/main/customer dropdown")
	
func doSomething(result ,response_code, headers3, body:PoolByteArray):
	var json = JSON.parse(body.get_string_from_utf8())
	var project_json3 = (json.result)
	var loop_limit3 = project_json3.size()
	var i = 0
	while i < loop_limit3:
		print(project_json3[i]["name"])
		cus_dropdown.add_item(project_json3[i]["name"])
		i = i+1



func _on_connect_pressed():
	var email = get_node("../TextureRect/file/mailtd").text
	var token = get_node("../TextureRect/file/tokentd").text
	var username = get_node("../TextureRect/file/ustetd").text
	var password = get_node("../TextureRect/file/passtd").text
	var auth=str("Basic ", Marshalls.utf8_to_base64(str(username, ":", password)))
	var headers3 = [
		"Authorization: "+auth,
		"X-AUTH-USER: "+email,
		"X-AUTH-TOKEN: "+token
	]
	self.connect("request_completed" , self , "doSomething")
	self.request("https://time.silentridge.io/api/customers", headers3 , true , HTTPClient.METHOD_GET)
	
	pass # Replace with function body.
