extends Node2D

var ctime = 0
var is_captcha_solved  = false
var is_captcha_active = false
onready var startposition = self.position

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if(is_captcha_active == true):
		ctime += delta
		if(ctime >= 600):
			get_node("../../watch_timer").time = 0
			get_node("../profile/HTTPRequestforstop").stop()
	
	if(is_captcha_solved == true):
		self.position = startposition
		set_process(false)
		is_captcha_solved = false
		
		

