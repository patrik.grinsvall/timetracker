extends HTTPRequest


func _ready():
	self.connect("request_completed", self ,"stop_timer")

func stop():
	var email = get_node("../TextureRect/file/mailtd").text
	var token = get_node("../TextureRect/file/tokentd").text
	var username = get_node("../TextureRect/file/ustetd").text
	var password = get_node("../TextureRect/file/passtd").text
	var auth=str("Basic ", Marshalls.utf8_to_base64(str(username, ":", password)))
	
	var header = [
		"Content-Type: application/json",
		"Authorization: "+auth,
		"X-AUTH-USER: "+email,
		"X-AUTH-TOKEN: "+token
	]
	self.request("https://time.silentridge.io/api/timesheets/349/stop" , header, true , HTTPClient.METHOD_PATCH)

func stop_timer():
	print("timer stopped")
