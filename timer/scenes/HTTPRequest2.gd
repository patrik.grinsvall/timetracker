extends HTTPRequest

onready var act_dropdown = get_node("../TextureRect/main/activity dd")

func _on_Button_pressed():
	#var username = get_node("../TextureRect/file/username").text
	var email = get_node("../TextureRect/file/mailtd").text
	var token = get_node("../TextureRect/file/tokentd").text
	var username = get_node("../TextureRect/file/ustetd").text
	var password = get_node("../TextureRect/file/passtd").text
	var auth=str("Basic ", Marshalls.utf8_to_base64(str(username, ":", password)))
	var headers2 = [
		"Authorization: "+auth,
		"X-AUTH-USER: "+email,
		"X-AUTH-TOKEN: "+token
	]
	self.connect("request_completed" , self , "doSomething")
	self.request("https://time.silentridge.io/api/activities", headers2 , true , HTTPClient.METHOD_GET)
	
func doSomething(result ,response_code, headers2, body:PoolByteArray):
	var json = JSON.parse(body.get_string_from_utf8())
	var project_json2 = (json.result)
	var loop_limit2 = project_json2.size()
	var i = 0
	while i < loop_limit2:
		print(project_json2[i]["name"])
		act_dropdown.add_item(project_json2[i]["name"])
		i = i+1
