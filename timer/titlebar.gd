extends Control

var following = false
var draggingsp = Vector2()

func _on_titlebar_gui_input(event):
	if event is InputEventMouseButton:
		if event.get_button_index() == 1:
			following = !following
			draggingsp = get_local_mouse_position()
	pass

func _process(_delta):
	if following:
		OS.set_window_position(OS.window_position + get_global_mouse_position() - draggingsp)


func _on_Button_pressed():
	get_tree().quit()
	pass # Replace with function body.
