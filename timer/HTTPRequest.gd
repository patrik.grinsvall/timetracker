extends HTTPRequest

var is_connected = false

onready var proj_dropdown = get_node("../TextureRect/main/project dropdown")

func _on_Button_pressed():
	var email = get_node("../TextureRect/file/mailtd").text
	var token = get_node("../TextureRect/file/tokentd").text
	var username = get_node("../TextureRect/file/ustetd").text
	var password = get_node("../TextureRect/file/passtd").text
	var auth=str("Basic ", Marshalls.utf8_to_base64(str(username, ":", password)))
	var headers = [
		"Authorization: "+auth,
		"X-AUTH-USER: "+email,
		"X-AUTH-TOKEN: "+token
	]
	self.connect("request_completed" , self , "doSomething")
	self.request("https://time.silentridge.io/api/projects", headers , true , HTTPClient.METHOD_GET)
	pass # Replace with function body.

func doSomething(result ,response_code, headers, body:PoolByteArray):
	var json = JSON.parse(body.get_string_from_utf8())
	var project_json = (json.result)
	var loop_limit1 = project_json.size()
	var i = 0
	is_connected = true
	while i < loop_limit1:
		print(project_json[i]["name"])
		proj_dropdown.add_item(project_json[i]["name"])
		i = i+1
