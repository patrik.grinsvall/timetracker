extends Label


var time = 0
var timer_on = false

func _process(delta):
	if(timer_on):
		time += delta
		
	var secs = fmod(time,60)
	var mins = fmod(time, 60*60) /60
	var hour = fmod(fmod(time,3600 * 60) / 3600,24)
	
	var time_passed = "%02d : %02d : %02d" %[hour,mins,secs]
	text = time_passed
	pass

func _on_CheckButton_toggled(button_pressed):
	var image = get_viewport().get_texture().get_data() 
	image.flip_y() 
	image.save_png("d:/work/screenshot.png")
	if(!button_pressed):
		timer_on = false
	if(button_pressed):
		timer_on = true
	pass
	
