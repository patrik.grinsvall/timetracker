extends Label


var time = 0
var timer_on = false
onready var captcha = get_node("../cholder")
var question_activator = true

func _process(delta):
	if(timer_on):
		time += delta
		
	var secs = fmod(time,60)
	var mins = fmod(time, 60*60) /60
	var hour = fmod(fmod(time,3600 * 60) / 3600,24)
	
	if(get_node("../cholder").is_captcha_solved == true):
		get_node("../cholder").position = Vector2(0,270)
	
	var time_passed = "%02d : %02d : %02d" %[hour,mins,secs]
	text = time_passed
	if(secs >= 1800):
		get_node("../cholder").position = Vector2(0,-270)
		captcha.show()
		if(question_activator == true):
			get_node("../cholder/capcha/question").questionair()
			question_activator = false
	pass
	
	


func _on_CheckButton_toggled(button_pressed):
	if(!button_pressed):
		get_node("../profile/HTTPRequestforstop").stop()
		timer_on = false
	if(button_pressed):
		if(get_node("../profile/HTTPRequest").is_connected == true):
			get_node("../profile/HTTPRequest4").post_operation()
			timer_on = true
	pass # Replace with function body.
